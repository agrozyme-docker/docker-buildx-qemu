#! /bin/bash
set -euxo pipefail

function main() {
  local bin="${PWD}/rootfs/usr/local/bin"
  local run="${bin}/docker-build.sh"

  chmod +x "${bin}"/*.sh
  ${run} build
}

main "$@"
