FROM docker.io/library/alpine
RUN apk add --no-cache bash
SHELL [ "/bin/bash", "-c" ]
COPY rootfs /

RUN set -euxo pipefail \
  && chmod +x /usr/local/bin/* \
  && /usr/local/bin/docker-build.sh build

ENTRYPOINT ["/sbin/tini-static", "--"]
CMD [ "/bin/bash" ]
